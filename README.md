#What is terraform

[Terraform](http://www.terraform.io/) provides a common configuration to launch infrastructure from physical and virtual servers to email and DNS providers. Once launched, [Terraform](http://www.terraform.io/) safely and efficiently changes infrastructure as the configuration is evolved.

Simple file based configuration gives you a single view of your entire infrastructure.

http://www.terraform.io/